﻿using System;
using System.Collections.Generic;

#nullable disable

namespace lab3.Model.ORM
{
    public partial class LinkAnimeChar
    {
        public int LinkId { get; set; }
        public int CharId { get; set; }
        public string AnimeName { get; set; }

        public virtual Anime AnimeNameNavigation { get; set; }
        public virtual Character Char { get; set; }
    }
}