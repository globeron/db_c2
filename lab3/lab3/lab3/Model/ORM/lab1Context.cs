﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace lab3.Model.ORM
{
    public partial class lab1Context : DbContext
    {
        public lab1Context()
        {
        }

        public lab1Context(DbContextOptions<lab1Context> options)
            : base(options)
        {
        }

        public virtual DbSet<Anime> Animes { get; set; }
        public virtual DbSet<Character> Characters { get; set; }
        public virtual DbSet<LinkAnimeChar> LinkAnimeChars { get; set; }
        public virtual DbSet<Studio> Studios { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseNpgsql("Host=localhost;Database=lab1;Username=postgres;Password=5175");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Russian_Russia.1251");

            modelBuilder.Entity<Anime>(entity =>
            {
                entity.HasKey(e => e.AnimeName)
                    .HasName("anime_pkey");

                entity.ToTable("anime");

                entity.Property(e => e.AnimeName)
                    .HasMaxLength(50)
                    .HasColumnName("animeName");

                entity.Property(e => e.Date).HasColumnName("date");

                entity.Property(e => e.Rating).HasColumnName("rating");

                entity.Property(e => e.StudioName)
                    .IsRequired()
                    .HasColumnType("character varying")
                    .HasColumnName("studioName");

                entity.HasOne(d => d.StudioNameNavigation)
                    .WithMany(p => p.Animes)
                    .HasForeignKey(d => d.StudioName)
                    .HasConstraintName("studioName_fkey");
            });

            modelBuilder.Entity<Character>(entity =>
            {
                entity.HasKey(e => e.CharId)
                    .HasName("character_pkey");

                entity.ToTable("character");

                entity.Property(e => e.CharId)
                    .HasColumnName("charID")
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.Age).HasColumnName("age");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(25)
                    .HasColumnName("name");

                entity.Property(e => e.Sex)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasColumnName("sex");
            });

            modelBuilder.Entity<LinkAnimeChar>(entity =>
            {
                entity.HasKey(e => e.LinkId)
                    .HasName("linkID_pkey");

                entity.ToTable("linkAnimeChar");

                entity.Property(e => e.LinkId)
                    .HasColumnName("linkID")
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.AnimeName)
                    .IsRequired()
                    .HasColumnType("character varying")
                    .HasColumnName("animeName");

                entity.Property(e => e.CharId).HasColumnName("charID");

                entity.HasOne(d => d.AnimeNameNavigation)
                    .WithMany(p => p.LinkAnimeChars)
                    .HasForeignKey(d => d.AnimeName)
                    .HasConstraintName("animeName_fkey");

                entity.HasOne(d => d.Char)
                    .WithMany(p => p.LinkAnimeChars)
                    .HasForeignKey(d => d.CharId)
                    .HasConstraintName("charID_fkey");
            });

            modelBuilder.Entity<Studio>(entity =>
            {
                entity.HasKey(e => e.Name)
                    .HasName("Studio_pkey");

                entity.ToTable("studio");

                entity.Property(e => e.Name)
                    .HasMaxLength(20)
                    .HasColumnName("name");

                entity.Property(e => e.Activity).HasColumnName("activity");

                entity.Property(e => e.NumOfTitles).HasColumnName("numOfTitles");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
