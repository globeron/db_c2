﻿using System;
using System.Collections.Generic;

#nullable disable

namespace lab3.Model.ORM
{
    public partial class Character
    {
        public Character()
        {
            LinkAnimeChars = new HashSet<LinkAnimeChar>();
        }

        public int CharId { get; set; }
        public string Name { get; set; }
        public string Sex { get; set; }
        public int Age { get; set; }

        public virtual ICollection<LinkAnimeChar> LinkAnimeChars { get; set; }
        public void printCharacter()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Id: ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(CharId);
            //
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Name: ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(Name);
            //
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Sex: ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(Sex);
            //
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Age: ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(Age);
        }
    }
}
