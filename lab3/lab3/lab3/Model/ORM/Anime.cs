﻿using System;
using System.Collections.Generic;

#nullable disable

namespace lab3.Model.ORM
{
    public partial class Anime
    {
        public Anime()
        {
            LinkAnimeChars = new HashSet<LinkAnimeChar>();
        }

        public string AnimeName { get; set; }
        public string StudioName { get; set; }
        public int Date { get; set; }
        public float Rating { get; set; }

        public virtual Studio StudioNameNavigation { get; set; }
        public virtual ICollection<LinkAnimeChar> LinkAnimeChars { get; set; }
        public void printAnime()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Name: ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(AnimeName);
            //
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Studio: ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(StudioName);
            //
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Date: ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(Date);
            //
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Rating: ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(Rating);
        }
    }
}
