﻿using System;
using System.Collections.Generic;

#nullable disable

namespace lab3.Model.ORM
{
    public partial class Studio
    {
        public Studio()
        {
            Animes = new HashSet<Anime>();
        }

        public string Name { get; set; }
        public bool Activity { get; set; }
        public int NumOfTitles { get; set; }

        public virtual ICollection<Anime> Animes { get; set; }
    }
}
