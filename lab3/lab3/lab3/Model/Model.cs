﻿using System;
using System.Collections.Generic;
using System.Text;
using Npgsql;
using lab3.Model.ORM;

namespace lab3.Model
{
    class Model
    {
        private NpgsqlConnection database;
        public Model()
        {
            if (database == null)
            {
                database = new NpgsqlConnection("Host = localhost; Username = postgres; Password = 5175; Database = lab1");
                database.Open();
            }
        }
        public Anime getAnimeByName(string name)
        {
            using (lab1Context db = new lab1Context())
            {
                return db.Animes.Find(name);
            }
        }
        public string addAnime(Anime newAnime)//в курсаче сделать throw
        {
            using (lab1Context db = new lab1Context())
            {
                string AnimeName = db.Animes.Add(newAnime).Entity.AnimeName;
                try
                {
                    db.SaveChanges();
                }
                catch (Exception)
                {

                    return null;
                }
                return AnimeName;
            }
        }
        public bool deleteAnime(string animeName)
        {
            using (lab1Context db = new lab1Context())
            {
                db.Animes.Remove(new Anime { AnimeName = animeName });
                try
                {
                    db.SaveChanges();
                }
                catch (Exception)
                {

                    return false;
                }
                return true;
            }
        }
        public Anime editAnime(Anime editedAnime)
        {
            using (lab1Context db = new lab1Context())
            {
                Anime Anime_ = db.Animes.Update(editedAnime).Entity;
                try
                {
                    db.SaveChanges();
                }
                catch (Exception)
                {

                    return null;
                }
                return Anime_;
            }
            }
        public void generatAnime(long num)
        {
            using var req = new NpgsqlCommand("INSERT INTO anime (\"animeName\", \"studioName\", \"date\", rating) SELECT chr(trunc(65 + random()*25)::int) || " +
                "chr(trunc(97 + random() * 25)::int) ||" +
                "chr(trunc(97 + random() * 25)::int) ||" +
                "chr(trunc(97 + random() * 25)::int) ||" +
                "chr(trunc(97 + random() * 25)::int) ||" +
                "chr(trunc(97 + random() * 25)::int) ||" +
                "chr(trunc(97 + random() * 25)::int) ||" +
                "chr(trunc(97 + random() * 25)::int), \"generatStudioName\"(), trunc(random() * 121 + 1900), trunc(random() * 9 + 1) FROM generate_series(1, @num)", database);
            req.Parameters.AddWithValue("@num", num);
            try
            {
                req.ExecuteNonQuery();
            }
            catch (Exception)
            {
                //(          
            }
        }
        public void generatCharacter(long num)
        {
            using var req = new NpgsqlCommand("INSERT INTO character (\"name\", \"sex\", age) SELECT chr(trunc(65 + random()*25)::int) ||"+
                "chr(trunc(97 + random() * 25)::int) ||"+
                "chr(trunc(97 + random() * 25)::int) ||"+
                "chr(trunc(97 + random() * 25)::int) ||"+
                "chr(trunc(97 + random() * 25)::int) ||"+
                "chr(trunc(97 + random() * 25)::int) ||"+
                "chr(trunc(97 + random() * 25)::int) ||"+
                "chr(trunc(97 + random() * 25)::int), \"generatGender\"(), trunc(random() * 50 + 1) FROM generate_series(1, @num)", database);
            req.Parameters.AddWithValue("@num", num);
            try
            {
                req.ExecuteNonQuery();
            }
            catch (Exception)
            {
                //(          
            }
        }
        public void generatLink(long num)
        {
            using var req = new NpgsqlCommand("INSERT INTO \"linkAnimeChar\"(\"charID\", \"animeName\") SELECT \"generatCharacter\"(), \"generatAnimeName\"() FROM generate_series(1, @num)", database);
            req.Parameters.AddWithValue("@num", num);
            req.ExecuteNonQuery();
            try
            {
                
            }
            catch (Exception)
            {
                //(          
            }
        }


        public int addCharacter(Character newCharacter)
        {
            using (lab1Context db = new lab1Context())
            {
                int charID = db.Characters.Add(newCharacter).Entity.CharId;
                try
                {
                    db.SaveChanges();
                }
                catch (Exception)
                {

                    return -1;
                }
                return charID;
            }
        }
        public bool deleteCharacter(int id)
        {
            using (lab1Context db = new lab1Context())
            {
                db.Characters.Remove(new Character { CharId = id });
                try
                {
                    db.SaveChanges();
                }
                catch (Exception)
                {

                    return false;
                }
                return true;
            }
        }
        public Character editCharacter(Character editedCharacter)
        {
            using (lab1Context db = new lab1Context())
            {
                Character Char_ = db.Characters.Update(editedCharacter).Entity;
                try
                {
                    db.SaveChanges();
                }
                catch (Exception)
                {

                    return null;
                }
                return Char_;
            }
        }
        public Character getCharacterById(int id)
        {
            using (lab1Context db = new lab1Context())
            {
                return db.Characters.Find(id);
            }
        }
        /// <summary>
        /// Поиск аниме по дате выхода, полу персонажа и возраста персонажа
        /// </summary>
        public List<Anime> searchAnime1(int ageL, int ageH, string sex, int dateL, int dateH)
        {
            using var req = new NpgsqlCommand("SELECT anime.\"animeName\", anime.\"studioName\", anime.date, anime.rating from \"anime\"" +
                                              "INNER JOIN \"character\" ON((character.age BETWEEN @ageL and @ageH) and(character.sex = @sex))" +
                                              "INNER JOIN \"linkAnimeChar\" ON(\"linkAnimeChar\".\"animeName\" = anime.\"animeName\" and(\"linkAnimeChar\".\"charID\" = character.\"charID\"))" +
                                              "WHERE(anime.date BETWEEN @dateL and @dateH)"
                                              , database);
            req.Parameters.AddWithValue("@ageL", ageL);
            req.Parameters.AddWithValue("@ageH", ageH);
            req.Parameters.AddWithValue("@sex", sex);
            req.Parameters.AddWithValue("@dateL", dateL);
            req.Parameters.AddWithValue("@dateH", dateH);
            using NpgsqlDataReader item = req.ExecuteReader();
            List<Anime> animeList = new List<Anime>();
            while (item.Read())
            {
                animeList.Add(new Anime { AnimeName = item.GetString(0), StudioName = item.GetString(1), Date = item.GetInt32(2), Rating = item.GetFloat(3) });
            }
            return animeList;

        }
        /// <summary>
        /// Поиск аниме по дате выхода, рейтингу и активности студии
        /// </summary>
        public List<Anime> searchAnime2(bool status, int dateL, int dateH, int ratingL, int ratingH)
        {
            using var req = new NpgsqlCommand("SELECT anime.\"animeName\", anime.\"studioName\", anime.date, anime.rating from anime "+ 
                                              "INNER JOIN studio ON(anime.\"studioName\" = studio.name)"+
                                              "WHERE(studio.activity = @actv) and(anime.date BETWEEN @dateL and @dateH) and(anime.rating BETWEEN @ratingL and @ratingH)"
                                              , database);
            req.Parameters.AddWithValue("@actv", status);
            req.Parameters.AddWithValue("@dateL", dateL);
            req.Parameters.AddWithValue("@dateH", dateH);
            req.Parameters.AddWithValue("@ratingL", ratingL);
            req.Parameters.AddWithValue("@ratingH", ratingH);
            using NpgsqlDataReader item = req.ExecuteReader();
            List<Anime> animeList = new List<Anime>();
            while (item.Read())
            {
                animeList.Add(new Anime { AnimeName = item.GetString(0), StudioName = item.GetString(1), Date = item.GetInt32(2), Rating = item.GetFloat(3) });
            }
            return animeList;
        }
        /// <summary>
        /// Поиск персонажа по названию аниме, полу персонажа и возраста
        /// </summary>
        public List<Character> searchCharacter1(string name, string sex, int ageL, int ageH)
        {
            using var req = new NpgsqlCommand("SELECT character.\"charID\", character.name, character.sex, character.age  from character "+
                                              "INNER JOIN anime ON(anime.\"animeName\" LIKE @like) "+
                                              "INNER JOIN \"linkAnimeChar\" ON(\"linkAnimeChar\".\"animeName\" = anime.\"animeName\" and(\"linkAnimeChar\".\"charID\" = character.\"charID\")) "+
                                              "WHERE(character.sex = @sex and(character.age BETWEEN @ageL and @ageH))"
                                              , database);
            req.Parameters.AddWithValue("@like", '%'+name+'%');
            req.Parameters.AddWithValue("@sex", sex);
            req.Parameters.AddWithValue("@ageL", ageL);
            req.Parameters.AddWithValue("@ageH", ageH);
            using NpgsqlDataReader item = req.ExecuteReader();
            List<Character> characterList = new List<Character>();
            while (item.Read())
            {
                characterList.Add(new Character { CharId = item.GetInt32(0), Name = item.GetString(1), Sex = item.GetString(2), Age = item.GetInt32(3) });
            }
            return characterList;
        }
    }
}