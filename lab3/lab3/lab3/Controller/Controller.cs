﻿using System;
using System.Collections.Generic;
using System.Text;
using lab3.Model.ORM;
using lab3.View;

namespace lab3.Controller
{
    class Controller
    {
        Model.Model Model_ = new Model.Model();
        View.View View_ = new View.View();
        public int exe()
        {
            while (true)
            {
                int mainMeny = View_.mainMenu();
                if (mainMeny == 1)//anime
                {
                    while (true)
                    {
                        int animeMenu = View_.animeMenu();
                        if (animeMenu == 1)//add
                        {
                            if (Model_.addAnime(View_.addAnimeMenu())!=null)
                            {
                                Console.ForegroundColor = ConsoleColor.Green;
                                Console.WriteLine("Successsfully");
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Error");
                            }
                            Console.ReadLine();
                        }
                        else if (animeMenu == 2)//edit
                        {
                            if (Model_.editAnime(View_.editAnimeMenu())!=null)
                            {
                                Console.ForegroundColor = ConsoleColor.Green;
                                Console.WriteLine("Successsfully");
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Error");
                            }
                            Console.ReadLine();
                        }
                        else if (animeMenu == 3)//delete
                        {
                            if(Model_.deleteAnime(View_.delteAnimeMenu()))
                            {
                                Console.ForegroundColor = ConsoleColor.Green;
                                Console.WriteLine("Successsfully");
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Error");
                            }
                            Console.ReadLine();
                        }
                        else if(animeMenu == 4)
                        {
                            break;
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Wrong number");
                            Console.ReadLine();
                        }
                    }
                }
                else if (mainMeny == 2)//character
                {
                    while (true)
                    {
                        int characterMenu = View_.characterMenu();
                        if (characterMenu == 1)//add
                        {
                            if (Model_.addCharacter(View_.addCharacterMenu()) != -1)
                            {
                                Console.ForegroundColor = ConsoleColor.Green;
                                Console.WriteLine("Successsfully");
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Error");
                            }
                            Console.ReadLine();
                        }
                        else if (characterMenu == 2)//edit
                        {
                            if (Model_.editCharacter(View_.editCharacterMenu()) != null)
                            {
                                Console.ForegroundColor = ConsoleColor.Green;
                                Console.WriteLine("Successsfully");
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Error");
                            }
                            Console.ReadLine();
                        }
                        else if (characterMenu == 3)//delete
                        {
                            if (Model_.deleteCharacter(View_.deleteCharacterMenu()))
                            {
                                Console.ForegroundColor = ConsoleColor.Green;
                                Console.WriteLine("Successsfully");
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Error");
                            }
                            Console.ReadLine();
                        }
                        else if (characterMenu == 4)
                        {
                            break;
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Wrong number");
                            Console.ReadLine();
                        }
                    }
                }
                else if (mainMeny == 3)//search
                {
                    while (true)
                    {
                        int searchMenu = View_.searchMenu();
                        if(searchMenu == 1)
                        {
                            string[] search = View_.searchMenu1();
                            List<Anime> list = Model_.searchAnime1(Convert.ToInt32(search[3]), Convert.ToInt32(search[4]), search[2], Convert.ToInt32(search[0]), Convert.ToInt32(search[1]));
                            foreach (var item in list)
                            {
                                item.printAnime();
                                Console.WriteLine();
                            }
                            Console.ReadLine();
                        }
                        else if(searchMenu == 2)
                        {
                            string[] search = View_.searchMenu2();
                            bool status = true;
                            if (Convert.ToInt32(search[0]) == 0)
                                status = false;
                            List<Anime> list = Model_.searchAnime2(status, Convert.ToInt32(search[1]), Convert.ToInt32(search[2]), Convert.ToInt32(search[3]), Convert.ToInt32(search[4]));
                            foreach (var item in list)
                            {
                                item.printAnime();
                                Console.WriteLine();
                            }
                            Console.ReadLine();
                        }
                        else if(searchMenu == 3)
                        {
                            string[] search = View_.searchMenu3();
                            List<Character> list = Model_.searchCharacter1(search[0], search[1], Convert.ToInt32(search[2]), Convert.ToInt32(search[3]));
                            foreach (var item in list)
                            {
                                item.printCharacter();
                                Console.WriteLine();
                            }
                            Console.ReadLine();
                        }
                        else if (searchMenu == 4)
                        {
                            break;
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Wrong number");
                            Console.ReadLine();
                        }
                    }
                }
                else if(mainMeny == 4)//generator
                {
                    long count = new int();
                    if((count = View_.generationMenu())>0)
                    {
                        Model_.generatAnime(count);
                        Model_.generatCharacter(count * 2);
                        Model_.generatLink(count / 2);
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Successsfully");
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Wrong number");
                        Console.ReadLine();
                    }
                }
                else if (mainMeny == 5)//exit
                {
                    return 0;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Wrong number");
                    Console.ReadLine();
                }
            }
            

        }
    }
}
