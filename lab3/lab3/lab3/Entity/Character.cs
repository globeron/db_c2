﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab2.Entity
{
    class Character
    {
        public int charID;
        public string name;
        public string sex;
        public int age;
        public Character(int id, string name, string sex, int age)
        {
            charID = id;
            this.name = name;
            this.sex = sex;
            this.age = age;
        }
       
    }
    
}
