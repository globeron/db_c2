﻿using System;
using System.Collections.Generic;
using kursa4.MVC.Model;
using System.IO;

namespace kursa4.Generation.ExportCSV
{
    class TagsCSV
    {
        /// <summary>
        /// mode == 0 - anime | 
        /// mode == 1 - character
        /// </summary>
        /// <param name="mode"></param>
        public static void GeneratCsv(List<TagQuant> list, int mode)
        {
            string writePath = "";
            if (mode == 0)
                writePath = @"C:\Users\glabe\Desktop\doc\report\db_c2\kursa4\graphic\‪tagsAnime.csv";
            else
                writePath = @"C:\Users\glabe\Desktop\doc\report\db_c2\kursa4\graphic\‪tagsCharacter.csv";
            try
            {
                using (StreamWriter sw = new StreamWriter(writePath, false, System.Text.Encoding.Default))
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        sw.WriteLine(list[i].tag.tag + "," + list[i].quantity);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
