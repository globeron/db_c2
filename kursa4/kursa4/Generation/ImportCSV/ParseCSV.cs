﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using kursa4.MVC.Model;
using Microsoft.VisualBasic.FileIO;

namespace kursa4.Generation.ImportCSV
{
    class ParseCSV
    {
        static public List<Anime> GetArrayFromCsv()
        {
            var reader = new StreamReader(File.OpenRead(@"C:\Users\glabe\Desktop\doc\report\bd\kursa4\table\anime.csv"));
            List<string> listA = new List<string>();
            List<string> listB = new List<string>();
            int name = -1;
            int score = -1;
            int rating = -1;
            int studio = -1;
            List<Anime> listAnime = new List<Anime>();
            if (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                StringReader csv_reader = new StringReader(line);
                TextFieldParser csv_parser = new TextFieldParser(csv_reader);
                csv_parser.SetDelimiters(",");
                csv_parser.HasFieldsEnclosedInQuotes = true;
                string[] values = csv_parser.ReadFields();
                for (int i = 0; i < values.Length; i++)
                {
                    if(values[i]=="name" || values[i] == "title")
                    {
                        name = i;
                    }
                    if(values[i] == "studio")
                    {
                        studio = i;
                    }
                    if(values[i] == "score")
                    {
                        score = i;
                    }
                    if(values[i] == "rating")
                    {
                        rating = i;
                    }
                }
            }
            if(!(name!=-1 && (score !=-1 || rating !=-1)))
            {
                return null;
            }
            while (!reader.EndOfStream)
            {
                bool check = false;
                var line = reader.ReadLine();
                StringReader csv_reader = new StringReader(line);
                TextFieldParser csv_parser = new TextFieldParser(csv_reader);
                csv_parser.SetDelimiters(",");
                csv_parser.HasFieldsEnclosedInQuotes = true;
                string[] values = csv_parser.ReadFields();
                //
                int num;
                float sing;
                if (!check)
                {
                    if (score != -1)
                    {
                        if (int.TryParse(values[score], out num) || float.TryParse(values[rating], NumberStyles.Float, CultureInfo.InvariantCulture.NumberFormat, out sing))
                        {
                            score = rating;
                        }
                    }
                    if (int.TryParse(values[rating], out num) || float.TryParse(values[rating], NumberStyles.Float, CultureInfo.InvariantCulture.NumberFormat, out sing))
                    {
                        score = rating;
                    }
                    if (score != rating)
                    {
                        return null;
                    }
                    check = true;
                }
                
                Anime anime;
                if (studio != -1)
                    anime = new Anime(0, values[name], float.Parse(values[score], CultureInfo.InvariantCulture.NumberFormat), values[studio]);
                else
                {
                    try
                    {
                        anime = new Anime(0, values[name], float.Parse(values[score], CultureInfo.InvariantCulture.NumberFormat), "");
                    }
                    catch (FormatException)
                    {
                        anime = new Anime(0, values[name], 0, null);
                    }
                }
                listAnime.Add(anime);
            }
            return listAnime;
        }
    }
}
