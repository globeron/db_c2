﻿using System;
using System.Collections.Generic;
using System.Text;

namespace kursa4.MVC.Model
{
    class Anime
    {
        public int animeId;
        public string animeName;
        public string studioName;
        public float rating = 0;
        public Anime(int id, string animeName, float score, string studioName)
        {
            animeId = id;
            this.animeName = animeName;
            this.studioName = studioName;
            rating = score;
        }
        public void printAnime()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Id: ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(animeId);
            //
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Name: ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(animeName);
            //
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Studio: ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(studioName);
            //
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Score: ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(rating);
        }
    }
}
