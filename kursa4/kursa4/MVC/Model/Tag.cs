﻿using System;
using System.Collections.Generic;
using System.Text;

namespace kursa4.MVC.Model
{
    class Tag
    {
        public int id;
        public string tag;
        public Tag(int id, string tag)
        {
            this.id = id;
            this.tag = tag;
        }
        public void printTag()
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write(id + ". ");
            //
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(tag);
            //
        }
    }
    struct TagQuant
    {
        public int quantity;
        public Tag tag;
        public TagQuant(int quantity, Tag tag)
        {
            this.quantity = quantity;
            this.tag = tag;
        }
    }
}
