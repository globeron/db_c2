﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using Npgsql;

namespace kursa4.MVC.Model
{
    class Model
    {
        private NpgsqlConnection database;
        public Model()
        {
            if (database == null)
            {
                database = new NpgsqlConnection("Host = localhost; Username = postgres; Password = 5175; Database = kursa4");
                database.Open();
            }
        }
        public Anime getAnimeById(int id)
        {
            using var req = new NpgsqlCommand("SELECT * from \"anime\" WHERE \"id\" = \'" + id + "\'", database);
            using NpgsqlDataReader item = req.ExecuteReader();
            if (item.Read())
            {
                return new Anime(item.GetInt32(0), item.GetString(1), item.GetFloat(2), item.GetString(3));
            }
            else
                return null;
        }
        public int addAnime(Anime newAnime)
        {
            {
                using var req1 = new NpgsqlCommand("SELECT COUNT(*) FROM anime WHERE anime.name = @animeName", database);
                req1.Parameters.AddWithValue("animeName", newAnime.animeName);
                using NpgsqlDataReader count = req1.ExecuteReader();
                count.Read();
                int num = count.GetInt32(0);
                if (count.GetInt32(0) > 0)
                    return -1;
            }
            using var req2 = new NpgsqlCommand("INSERT INTO anime(name, score, studio) VALUES(@animeName, @score, NULLIF(@studio,'')::text) RETURNING anime.id", database);
            req2.Parameters.AddWithValue("animeName", newAnime.animeName);
            req2.Parameters.AddWithValue("studio", newAnime.studioName);
            req2.Parameters.AddWithValue("score", newAnime.rating);
            try
            {
                using NpgsqlDataReader item = req2.ExecuteReader();
                if (item.Read())
                {
                    return item.GetInt32(0);
                }
            }
            catch (Exception)
            {
                return -1;
            }
            return -1;
        }
        public bool deleteAnime(int animeId)
        {
            bool status = true;
            using var req = new NpgsqlCommand("DELETE from \"anime\" WHERE \"id\" = \'" + animeId + "\'", database);
            req.Prepare();
            if (req.ExecuteNonQuery() <= 0)
                status = false;
            return status;
        }
        public Anime editAnime(Anime editedAnime)
        {
            using var req = new NpgsqlCommand("UPDATE \"anime\" SET name = @name, \"studio\" = @studio, score = @rating WHERE \"id\" = \'" + editedAnime.animeId + "\'", database);
            req.Parameters.AddWithValue("@name", editedAnime.animeName);
            req.Parameters.AddWithValue("@studio", editedAnime.studioName);
            req.Parameters.AddWithValue("@rating", editedAnime.rating);
            req.Prepare();
            if (req.ExecuteNonQuery() == -1)
                return null;
            using var req2 = new NpgsqlCommand("SELECT * FROM \"anime\" WHERE \"id\" = \'" + editedAnime.animeId + "\'", database);
            using NpgsqlDataReader item = req2.ExecuteReader();
            if (item.Read())
                return new Anime(item.GetInt32(0), item.GetString(1), Convert.ToSingle(item.GetDouble(2)), item.GetString(3));
            return null;
        }


        public int addCharacter(Character newCharacter)
        {
            using var req = new NpgsqlCommand("INSERT INTO character(name, age, gender) VALUES(@name, @age, @gender)  RETURNING character.id", database);
            req.Parameters.AddWithValue("name", newCharacter.name);
            req.Parameters.AddWithValue("gender", newCharacter.sex);
            req.Parameters.AddWithValue("age", newCharacter.age);
            req.Prepare();
            try
            {
                using NpgsqlDataReader item = req.ExecuteReader();
                if (item.Read())
                {
                    return item.GetInt32(0);
                }
            }
            catch (PostgresException)
            {
                return -1;
            }
            return -1;
        }
        public bool deleteCharacter(int id)
        {
            bool status = true;
            using var req = new NpgsqlCommand("DELETE from \"character\" WHERE \"id\" = " + id, database);
            if (req.ExecuteNonQuery() == -1)
                status = false;
            return status;
        }
        public Character editCharacter(Character editedCharacter)
        {
            using var req = new NpgsqlCommand("UPDATE \"character\" SET name = @name, gender = @gender, age = @age WHERE \"id\" = " + editedCharacter.charID, database);
            req.Parameters.AddWithValue("@name", editedCharacter.name);
            req.Parameters.AddWithValue("@gender", editedCharacter.sex);
            req.Parameters.AddWithValue("@age", editedCharacter.age);
            if (req.ExecuteNonQuery() == -1)
                return null;
            using var req2 = new NpgsqlCommand("SELECT * from \"character\" WHERE \"id\" = " + editedCharacter.charID, database);
            using NpgsqlDataReader item = req2.ExecuteReader();
            if (item.Read())
                return new Character(item.GetInt32(0), item.GetString(1), item.GetInt32(2), item.GetString(3));
            return null;
        }
        public Character getCharacterById(int id)
        {
            using var req = new NpgsqlCommand("SELECT * from \"character\" WHERE \"id\" = " + id, database);
            using NpgsqlDataReader item = req.ExecuteReader();
            if (item.Read())
            {
                return new Character(item.GetInt32(0), item.GetString(1), Convert.ToInt32(item.GetString(2)), item.GetString(3));
            }
            else
                return null;
        }
        /// <summary>
        /// Поиск персонажей по имени
        /// </summary>
        ///

        public List<Character> searchCharacterByName(string name)
        {
            using var req = new NpgsqlCommand("SELECT * FROM \"character\"" +
                                                "WHERE character.name LIKE @like"
                                              , database);
            req.Parameters.AddWithValue("@like", '%' + name + '%');
            using NpgsqlDataReader item = req.ExecuteReader();
            List<Character> characterList = new List<Character>();
            while (item.Read())
            {
                characterList.Add(new Character(item.GetInt32(0), item.GetString(1), Convert.ToInt32(item.GetString(2)), item.GetString(3)));
            }
            return characterList;
        }
        /// <summary>
        /// Поиск аниме по имени
        /// </summary>
        ///

        public List<Anime> searchAnimeByName(string name)
        {
            using var req = new NpgsqlCommand("SELECT * FROM \"anime\"" +
                                                "WHERE anime.name LIKE @like"
                                              , database);
            req.Parameters.AddWithValue("@like", '%' + name + '%');
            using NpgsqlDataReader item = req.ExecuteReader();
            List<Anime> AnimeList = new List<Anime>();
            while (item.Read())
            {
                try
                {
                    AnimeList.Add(new Anime(item.GetInt32(0), item.GetString(1), Convert.ToSingle(item.GetDouble(2)), item.GetString(3)));
                }
                catch (InvalidCastException)
                {
                    AnimeList.Add(new Anime(item.GetInt32(0), item.GetString(1), Convert.ToSingle(item.GetDouble(2)), ""));
                }
            }
            return AnimeList;
        }

        /// <summary>
        /// Поиск тегов персонажа по его айди
        /// </summary>
        public List<Tag> searchCharacterTags(int id)
        {
            using var req = new NpgsqlCommand("SELECT \"tagsCharacter\".id ,\"tagsCharacter\".tag  from \"character\"" +
                                                "INNER JOIN \"tagsCharacterLink\" ON(character.id = \"tagsCharacterLink\".character_id)" +
                                                "INNER JOIN \"tagsCharacter\" ON(\"tagsCharacter\".id = \"tagsCharacterLink\".tag_id)" +
                                                "WHERE character.id = @id"
                                                , database);
            req.Parameters.AddWithValue("@id", id);
            using NpgsqlDataReader item = req.ExecuteReader();
            List<Tag> TagsList = new List<Tag>();
            while (item.Read())
            {
                TagsList.Add(new Tag(item.GetInt32(0), item.GetString(1)));
            }
            return TagsList;
        }
        /// <summary>
        /// Поиск тегов аниме по его айди
        /// </summary>
        public List<Tag> searchAnimeTags(int id)
        {
            using var req = new NpgsqlCommand("SELECT \"tagsAnime\".id ,\"tagsAnime\".tag  from \"anime\"" +
                                                "INNER JOIN \"tagsAnimeLink\" ON(anime.id = \"tagsAnimeLink\".anime_id)" +
                                                "INNER JOIN \"tagsAnime\" ON(\"tagsAnime\".id = \"tagsAnimeLink\".tag_id)" +
                                                "WHERE anime.id = @id"
                                                , database);
            req.Parameters.AddWithValue("@id", id);
            using NpgsqlDataReader item = req.ExecuteReader();
            List<Tag> TagsList = new List<Tag>();
            while (item.Read())
            {
                TagsList.Add(new Tag(item.GetInt32(0), item.GetString(1)));
            }
            return TagsList;
        }
        public List<Anime> searchSimular(Anime anime)
        {
            List<Tag> TagList = searchAnimeTags(anime.animeId);
            List<Anime> AnimeList = new List<Anime>();
            bool flag = false;
            string reqStr = "SELECT anime.id, anime.name, anime.score, anime.studio, COUNT(tag) as tags FROM \"anime\"" +
                            "INNER JOIN \"tagsAnimeLink\" ON(anime.id = \"tagsAnimeLink\".anime_id AND(";

            foreach (var item in TagList)
            {
                reqStr += "\"tagsAnimeLink\".tag_id = " + item.id + " OR ";
                flag = true;
            }
            if (flag)
                reqStr = reqStr.Remove(reqStr.Length - 3);
            reqStr += "))";
            reqStr += " INNER JOIN \"tagsAnime\" ON(\"tagsAnime\".id = \"tagsAnimeLink\".tag_id) " +
                            "GROUP BY anime.id " +
                            "ORDER BY tags DESC";
            using var req = new NpgsqlCommand(reqStr, database);
            using NpgsqlDataReader items = req.ExecuteReader();
            while (items.Read())
            {
                try
                {
                    AnimeList.Add(new Anime(items.GetInt32(0), items.GetString(1), Convert.ToSingle(items.GetDouble(2)), items.GetString(3)));
                }
                catch (InvalidCastException)
                {
                    AnimeList.Add(new Anime(items.GetInt32(0), items.GetString(1), Convert.ToSingle(items.GetDouble(2)), ""));
                }
            }
            return AnimeList;
        }
        public List<Character> searchSimular(Character character)
        {
            List<Tag> TagList = searchCharacterTags(character.charID);
            List<Character> CharacterList = new List<Character>();
            bool flag = false;
            string reqStr = "SELECT character.id, character.name, character.age, character.gender, COUNT(tag) as tags FROM \"character\"" +
                            "INNER JOIN \"tagsCharacterLink\" ON(character.id = \"tagsCharacterLink\".character_id AND(";

            foreach (var item in TagList)
            {
                reqStr += "\"tagsCharacterLink\".tag_id = " + item.id + " OR ";
                flag = true;
            }
            if (flag)
                reqStr = reqStr.Remove(reqStr.Length - 3);
            reqStr += "))";
            reqStr += " INNER JOIN \"tagsCharacter\" ON(\"tagsCharacter\".id = \"tagsCharacterLink\".tag_id) " +
                            "GROUP BY character.id " +
                            "ORDER BY tags DESC";
            using var req = new NpgsqlCommand(reqStr, database);
            using NpgsqlDataReader items = req.ExecuteReader();
            while (items.Read())
            {
                CharacterList.Add(new Character(items.GetInt32(0), items.GetString(1), Convert.ToInt32(items.GetString(2)), items.GetString(3)));
            }
            return CharacterList;
        }
        public List<TagQuant> popularAnimeTags()
        {
            List<TagQuant> tagList = new List<TagQuant>();
            using var req = new NpgsqlCommand("SELECT \"tagsAnime\".id, tag, COUNT(tag)as quantity  FROM \"tagsAnime\" " +
                                              "INNER JOIN \"tagsAnimeLink\" ON(\"tagsAnime\".id = \"tagsAnimeLink\".tag_id) " +
                                              "GROUP BY \"tagsAnime\".id " +
                                              "ORDER BY quantity DESC"
                                              , database);
            using NpgsqlDataReader items = req.ExecuteReader();
            while (items.Read())
            {
                tagList.Add(new TagQuant(items.GetInt32(2), new Tag(items.GetInt32(0), items.GetString(1))));
            }
            return tagList;
        }
        public List<TagQuant> popularCharacterTags()
        {
            List<TagQuant> tagList = new List<TagQuant>();
            using var req = new NpgsqlCommand("SELECT \"tagsCharacter\".id, tag, COUNT(tag)as quantity  FROM \"tagsCharacter\" " +
                                              "INNER JOIN \"tagsCharacterLink\" ON(\"tagsCharacter\".id = \"tagsCharacterLink\".tag_id) " +
                                              "GROUP BY \"tagsCharacter\".id " +
                                              "ORDER BY quantity DESC"
                                              , database);
            using NpgsqlDataReader items = req.ExecuteReader();
            while (items.Read())
            {
                tagList.Add(new TagQuant(items.GetInt32(2), new Tag(items.GetInt32(0), items.GetString(1))));
            }
            return tagList;
        }
        public void backup()
        {
            String dumpCommand = "\"" + @"C:\Program Files\PostgreSQL\12\bin\pg_dump.exe" + "\"" + " -Fc" + " -h " + "localhost" + " -p " + 5432 + " -d " + "kursa4" + " -U " + "postgres" + "";
            String passFileContent = "" + "localhost" + ":" + 5432 + ":" + "kursa4" + ":" + "postgres" + ":" + 5175 + "";

            String batFilePath = Path.Combine(
                Path.GetTempPath(),
                Guid.NewGuid().ToString() + ".bat");

            String passFilePath = Path.Combine(
                Path.GetTempPath(),
                Guid.NewGuid().ToString() + ".conf");

            try
            {
                String batchContent = "";
                batchContent += "@" + "set PGPASSFILE=" + passFilePath + "\n";
                batchContent += "@" + dumpCommand + "  > " + "\"" + @"C:\Users\glabe\Desktop\doc\report\db_c2\kursa4\kursa4\kursa4.sql" + "\"" + "\n";

                File.WriteAllText(
                    batFilePath,
                    batchContent,
                    Encoding.ASCII);

                File.WriteAllText(
                    passFilePath,
                    passFileContent,
                    Encoding.ASCII);

                if (File.Exists(@"C:\Users\glabe\Desktop\doc\report\db_c2\kursa4\kursa4\kursa4.sql"))
                    File.Delete(@"C:\Users\glabe\Desktop\doc\report\db_c2\kursa4\kursa4\kursa4.sql");

                ProcessStartInfo oInfo = new ProcessStartInfo(batFilePath);
                oInfo.UseShellExecute = false;
                oInfo.CreateNoWindow = true;

                using (Process proc = System.Diagnostics.Process.Start(oInfo))
                {
                    proc.WaitForExit();
                    proc.Close();
                }
            }
            finally
            {
                if (File.Exists(batFilePath))
                    File.Delete(batFilePath);

                if (File.Exists(passFilePath))
                    File.Delete(passFilePath);
            }
        }
        public void restore()
        {

            StreamWriter sw = new StreamWriter("DBRestore.bat");
            String strSB = "\"C:\\Program Files\\PostgreSQL\\12\\bin\\pg_restore.exe\" --host \"localhost\" --port \"5432\" --username \"postgres\" -n public --dbname \"kursa4\" --exit-on-error --verbose --no-acl --no-owner \"C:\\Users\\glabe\\Desktop\\doc\\report\\db_c2\\kursa4\\kursa4\\kursa4.sql\"";
            sw.WriteLine(strSB);
            sw.Dispose();
            sw.Close();
            Process processDB = Process.Start("DBRestore.bat");
        }
    }
}
