﻿using System;
using System.Collections.Generic;
using System.Text;

namespace kursa4.MVC.Model
{
    class Character
    {
        public int charID;
        public string name;
        public string sex;
        public int age;
        public Character(int id, string name, int age, string gender)
        {
            charID = id;
            this.name = name;
            this.sex = gender;
            this.age = age;
        }
        public void printCharacter()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Id: ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(charID);
            //
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Name: ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(name);
            //
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Gender: ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(sex);
            //
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Age: ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(age);
        }
    }
}
