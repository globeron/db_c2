﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using kursa4.MVC.Model;

namespace kursa4.MVC.Controller
{
    class Controller
    {
        Model.Model Model_ = new Model.Model();
        View.View View_ = new View.View();
        public int exe()
        {
            while (true)
            {
                int mainMeny = View_.mainMenu();
                if (mainMeny == 1)//anime
                {
                    while (true)
                    {
                        int animeMenu = View_.animeMenu();
                        if (animeMenu == 1)//add
                        {
                            if (Model_.addAnime(View_.addAnimeMenu()) != -1)
                            {
                                Console.ForegroundColor = ConsoleColor.Green;
                                Console.WriteLine("Successsfully");
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Error");
                            }
                            Console.ReadLine();
                        }
                        else if (animeMenu == 2)//search
                        {
                            Console.ForegroundColor = ConsoleColor.Gray;
                            Console.WriteLine("Select anime");
                            Console.ForegroundColor = ConsoleColor.Green;
                            List<Anime> AnimeList = Model_.searchAnimeByName(View_.searchAnimeByName());
                            Start:
                            int res = View_.searchAnimeMenu(AnimeList);
                            if (res == -1)
                                break;
                            int command = View_.searchMenuMenu();
                            if(command == 1)//edit
                            {
                                Model_.editAnime(View_.editAnimeMenu(AnimeList[res].animeId));
                            }
                            else if(command == 2)//delete
                            {
                                Model_.deleteAnime(AnimeList[res].animeId);
                            }
                            else if (command == 3)//view tags
                            {
                                List<Tag> Tags = Model_.searchAnimeTags(AnimeList[res].animeId);
                                Console.Clear();
                                foreach (var item in Tags)
                                {
                                    item.printTag();
                                }
                            }
                            else if (command == 4)//simular
                            {
                                AnimeList = Model_.searchSimular(AnimeList[res]);
                                goto Start;
                            }
                            else if (command == 5)//back
                            {
                                break;
                            }
                            Console.ReadLine();
                        }
                        else if (animeMenu == 3)//popular tags
                        {
                            Console.Clear();
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("1. Console\n2. Web\n3. Back");
                            string res = Console.ReadLine();
                            if (res == "1")
                                View_.showPopularTags(Model_.popularAnimeTags());
                            else if (res == "2")
                            {
                                Generation.ExportCSV.TagsCSV.GeneratCsv(Model_.popularAnimeTags(), 0);
                                var proc = Process.Start(@"cmd.exe ", @"/c C:\Users\glabe\Desktop\doc\report\db_c2\kursa4\graphic\tagsAnime.html");
                            }
                            else if (res == "3")
                            {
                                break;
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Wrong input");
                            }
                        }
                        else if (animeMenu == 4)
                        {
                            break;
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Wrong number");
                            Console.ReadLine();
                        }
                    }
                }
                else if (mainMeny == 2)//character
                {
                    while (true)
                    {
                        int characterMenu = View_.characterMenu();
                        if (characterMenu == 1)//add
                        {
                            if (Model_.addCharacter(View_.addCharacterMenu()) != -1)
                            {
                                Console.ForegroundColor = ConsoleColor.Green;
                                Console.WriteLine("Successsfully");
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Error");
                            }
                            Console.ReadLine();
                        }
                        else if (characterMenu == 2)//search
                        {
                            Console.ForegroundColor = ConsoleColor.Gray;
                            Console.WriteLine("Select character");
                            Console.ForegroundColor = ConsoleColor.Green;
                            List<Character> CharacterList = Model_.searchCharacterByName(View_.searchCharacterByName());
                            Start:
                            int res = View_.searchCharacterMenu(CharacterList);
                            if (res == -1)
                                break;
                            int command = View_.searchMenuMenu();
                            if (command == 1)//edit
                            { 
                                Model_.editCharacter(View_.editCharacterMenu(CharacterList[res].charID));
                            }
                            else if (command == 2)//delete
                            {
                                Model_.deleteCharacter(CharacterList[res].charID);
                            }
                            else if (command == 3)//view tags
                            {
                                List<Tag> Tags = Model_.searchCharacterTags(CharacterList[res].charID);
                                Console.Clear();
                                foreach (var item in Tags)
                                {
                                    item.printTag();
                                }
                            }
                            else if (command == 4)//simular
                            {
                                CharacterList = Model_.searchSimular(CharacterList[res]);
                                goto Start;
                            }
                            else if (command == 5)//back
                            {
                                break;
                            }
                            Console.ReadLine();
                        }
                        else if (characterMenu == 3)//popular tags
                        {
                            Console.Clear();
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("1. Console\n2. Web\n3. Back");
                            string res = Console.ReadLine();
                            if (res == "1")
                                View_.showPopularTags(Model_.popularCharacterTags());
                            else if (res == "2")
                            {
                                Generation.ExportCSV.TagsCSV.GeneratCsv(Model_.popularCharacterTags(), 1);
                                var proc = Process.Start(@"cmd.exe ", @"/c C:\Users\glabe\Desktop\doc\report\db_c2\kursa4\graphic\tagsCharacter.html");
                            }
                            else if (res == "3")
                            {
                                break;
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Wrong input");
                            }
                        }
                        else if (characterMenu == 4)
                        {
                            break;
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Wrong number");
                            Console.ReadLine();
                        }
                    }
                }
                else if (mainMeny == 3)//backup
                {
                    Model_.backup();
                }
                else if (mainMeny == 4)//backup
                {
                    Model_.restore();
                }
                else if (mainMeny == 5)//import csv
                {
                    Console.WriteLine("Reading...");
                    List<Anime> listAnime = Generation.ImportCSV.ParseCSV.GetArrayFromCsv();
                    if (listAnime == null)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Error while importing csv file");
                        Console.ReadLine();
                    }
                    else
                    {
                        Console.WriteLine("Importing...");
                        foreach (var item in listAnime)
                        {
                            Model_.addAnime(item);
                        }
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Successful");
                        Console.ReadLine();
                    }
                }
                else if (mainMeny == 6)//exit
                {
                    return 0;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Wrong number");
                    Console.ReadLine();
                }
            }


        }
    }
}
