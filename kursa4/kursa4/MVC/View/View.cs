﻿using System;
using kursa4.MVC.Model;
using System.Text;
using System.Collections.Generic;

namespace kursa4.MVC.View
{
    class View
    {
        public int mainMenu()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("1. ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Anime");
            //
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("2. ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Character");
            //
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("3. ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Backup");
            //
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("4. ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Restore");
            //
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("5. ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Import data");
            //
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("6. ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Exit");
            //
            int int_input;
            string input = Console.ReadLine();
            try
            {
                int_input = Convert.ToInt32(input);
            }
            catch (Exception)
            {
                return 0;
            }
            return int_input;
        }
        public int animeMenu()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("1. ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Add anime");
            //
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("2. ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Search anime");
            //
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("3. ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Popular tags");
            //
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("4. ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Back");
            int int_input;
            string input = Console.ReadLine();
            try
            {
                int_input = Convert.ToInt32(input);
            }
            catch (Exception)
            {
                return 0;
            }
            return int_input;
        }
        public int characterMenu()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("1. ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Add character");
            //
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("2. ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Search character");
            //
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("3. ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Popular tags");
            //
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("4. ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Back");
            //
            int int_input;
            string input = Console.ReadLine();
            try
            {
                int_input = Convert.ToInt32(input);
            }
            catch (Exception)
            {
                return 0;
            }
            return int_input;
        }

        public Anime addAnimeMenu()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Write anime name: ");
            string animeName = Console.ReadLine();
            Console.Write("Write score: ");
            string rating = Console.ReadLine();
            Console.Write("Write studio: ");
            string studioName = Console.ReadLine(); 
            int ratingInt = new int();
            try
            {
                ratingInt = Convert.ToInt32(rating);
            }
            catch (Exception)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("Wrong input type");
                Console.ReadLine();
                addAnimeMenu();
            }
            return new Anime(0, animeName, ratingInt, studioName);
        }
        public Character addCharacterMenu()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Write character name: ");
            string name = Console.ReadLine();
            Console.Write("Write character age: ");
            string age = Console.ReadLine();
            Console.Write("Write character gender: ");
            string sex = Console.ReadLine();
            int ageInt = new int();
            try
            {
                ageInt = Convert.ToInt32(age);
            }
            catch (Exception)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("Wrong input type");
                Console.ReadLine();
                addCharacterMenu();
            }
            return new Character(0, name, ageInt, sex);
        }
        public string searchAnimeByName()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Write anime name: ");
            return Console.ReadLine();
        }
        public int searchAnimeMenu(List<Anime> AnimeList)
        {
            Console.Clear();
            for (int i = 0; i<AnimeList.Count && i<6; i++)
            {
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.Write(i+1 + ". ");
                AnimeList[i].printAnime();
            }
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("0. Back");
            string num = Console.ReadLine();
            int numInt = new int();
            try
            {
                numInt = Convert.ToInt32(num);
            }
            catch (Exception)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("Wrong input type");
                Console.ReadLine();
                searchAnimeMenu(AnimeList);
            }
            if(numInt>6 || numInt> AnimeList.Count)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("Wrong input num");
                Console.ReadLine();
                searchAnimeMenu(AnimeList);
            }
            return numInt-1;
        }
        public void showPopularTags(List<TagQuant> TagList)
        {
            Console.Clear();
            for (int i = 0; i < TagList.Count && i < 10; i++)
            {
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.Write(i + 1 + ". ");
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(TagList[i].tag.tag + '|' + TagList[i].quantity);
            }
            Console.ReadLine();
        }
        public int searchMenuMenu()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("1. Edit\n2. Delete\n3. View tags\n4. Find similar\n5. Back");
            string res = Console.ReadLine();
            int resInt = new int();
            try
            {
                resInt = Convert.ToInt32(res);
            }
            catch (Exception)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("Wrong input type");
                Console.ReadLine();
                addCharacterMenu();
            }
            if (resInt > 5)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("Wrong input num");
                Console.ReadLine();
                searchMenuMenu();
            }
            return resInt;
        }
        public int searchCharacterMenu(List<Character> CharacterList)
        {
            Console.Clear();
            for (int i = 0; i < CharacterList.Count && i < 6; i++)
            {
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.Write(i + 1 + ". ");
                CharacterList[i].printCharacter();
            }
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("0. Back");
            string num = Console.ReadLine();
            int numInt = new int();
            try
            {
                numInt = Convert.ToInt32(num);
            }
            catch (Exception)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("Wrong input type");
                Console.ReadLine();
                searchCharacterMenu(CharacterList);
            }
            if (numInt > 6 || numInt > CharacterList.Count)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("Wrong input num");
                Console.ReadLine();
                searchCharacterMenu(CharacterList);
            }
            return numInt-1;
        }
        public string searchCharacterByName()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Write character name: ");
            return Console.ReadLine();
        }
        public Anime editAnimeMenu(int id)
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Write anime name: ");
            string animeName = Console.ReadLine();
            Console.Write("Write score: ");
            string rating = Console.ReadLine();
            Console.Write("Write studio: ");
            string studioName = Console.ReadLine();
            int ratingInt = new int();
            try
            {
                ratingInt = Convert.ToInt32(rating);
            }
            catch (Exception)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("Wrong input type");
                Console.ReadLine();
                editAnimeMenu(id);
            }
            return new Anime(id, animeName, ratingInt, studioName);
        }
        public Character editCharacterMenu(int id)
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Write character name: ");
            string name = Console.ReadLine();
            Console.Write("Write character gender: ");
            string sex = Console.ReadLine();
            Console.Write("Write character age: ");
            string age = Console.ReadLine();
            int ageInt = new int();
            try
            {
                ageInt = Convert.ToInt32(age);
            }
            catch (Exception)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("Wrong input type");
                Console.ReadLine();
                editCharacterMenu(id);
            }
            return new Character(id, name, ageInt, sex);
        }
    }
}
