﻿using System;
using System.Collections.Generic;
using System.Text;
using lab2.Entity;

namespace lab2.View
{
    class View
    {
        public int mainMenu()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("1. ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Anime");
            //
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("2. ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Character");
            //
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("3. ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Search");
            //
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("4. ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Exit");
            //
            int int_input;
            string input = Console.ReadLine();
            try
            {
                int_input = Convert.ToInt32(input);
            }
            catch (Exception)
            {
                return 0;
            }
            return int_input;
        }
        public int animeMenu()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("1. ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Add anime");
            //
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("2. ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Edit anime");
            //
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("3. ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Delete anime");
            //
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("4. ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Back");
            int int_input;
            string input = Console.ReadLine();
            try
            {
                int_input = Convert.ToInt32(input);
            }
            catch (Exception)
            {
                return 0;
            }
            return int_input;
        }
        public int characterMenu()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("1. ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Add character");
            //
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("2. ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Edit character");
            //
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("3. ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Delete character");
            //
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("4. ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Back");
            //
            int int_input;
            string input = Console.ReadLine();
            try
            {
                int_input = Convert.ToInt32(input);
            }
            catch (Exception)
            {
                return 0;
            }
            return int_input;
        }
        public int searchMenu()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("1. ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Search anime by date, charater gender and age: ");
            //
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("2. ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Search anime by date, rating and studio activity: ");
            //
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("3. ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Search character by gender, age and anime name: ");
            //
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("4. ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Back");
            //
            int int_input;
            string input = Console.ReadLine();
            try
            {
                int_input = Convert.ToInt32(input);
            }
            catch (Exception)
            {
                return 0;
            }
            return int_input;
        }
        public string[] searchMenu2()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Write lower date limit : ");
            string dateL = Console.ReadLine();
            //
            Console.WriteLine("Write upper date limit : ");
            string dateH = Console.ReadLine();
            //
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Write lower rating limit: ");
            string ratingL = Console.ReadLine();
            //
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Write upper rating limit: ");
            string ratingH = Console.ReadLine();
            //
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Write activity status(0 for false || <0 or >0 for true): ");
            string status = Console.ReadLine();
            //
            try
            {
                Convert.ToInt32(dateL);
                Convert.ToInt32(dateH);
                Convert.ToInt32(ratingL);
                Convert.ToInt32(ratingH);
                Convert.ToInt32(status);
            }
            catch (Exception)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("Wrong input type");
                Console.ReadLine();
                searchMenu2();
            }
            return new string[] {status, dateL, dateH, ratingL, ratingH};
        }
        public string[] searchMenu1()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Write lower date limit: ");
            string dateL = Console.ReadLine();
            //
            Console.WriteLine("Write upper date limit: ");
            string dateH = Console.ReadLine();
            //
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Write gender: ");
            string sex = Console.ReadLine();
            //
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Write lower age limit: ");
            string ageL = Console.ReadLine();
            //
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Write upper age limit: ");
            string ageH = Console.ReadLine();
            try
            {
                Convert.ToInt32(dateL);
                Convert.ToInt32(dateH);
                Convert.ToInt32(ageL);
                Convert.ToInt32(ageH);
            }
            catch (FormatException)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("Wrong input type");
                Console.ReadLine();
                searchMenu1();
            }
            return new string[] { dateL, dateH, sex, ageL, ageH };
        }
        public string[] searchMenu3()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Write anime name: ");
            string name = Console.ReadLine();
            //
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Write character gender: ");
            string sex = Console.ReadLine();
            //
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Write character lower age limit: ");
            string ageL = Console.ReadLine();
            //
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Write character upper age limit: ");
            string ageH = Console.ReadLine();
            try
            {
                Convert.ToInt32(ageL);
                Convert.ToInt32(ageH);
            }
            catch (Exception)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("Wrong input type");
                Console.ReadLine();
                searchMenu3();
            }
            return new string[] { name, sex, ageL, ageH };
        }
        public Anime addAnimeMenu()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Write anime name: ");
            string animeName = Console.ReadLine();
            Console.Write("Write studio name: ");
            string studioName = Console.ReadLine();
            Console.Write("Write date: ");
            string date = Console.ReadLine();
            int dateInt = new int();
            Console.Write("Write rating: ");
            string rating = Console.ReadLine();
            int ratingInt = new int();
            try
            {
                dateInt = Convert.ToInt32(date);
                ratingInt = Convert.ToInt32(rating);
            }
            catch (Exception)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("Wrong input type");
                Console.ReadLine();
                addAnimeMenu();
            }
            return new Anime(animeName, studioName, dateInt, ratingInt);
        }
        public Character addCharacterMenu()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Write character name: ");
            string name = Console.ReadLine();
            Console.Write("Write character sex: ");
            string sex = Console.ReadLine();
            Console.Write("Write character age: ");
            string age = Console.ReadLine();
            int ageInt = new int();
            try
            {
                ageInt = Convert.ToInt32(age);            
            }
            catch (Exception)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("Wrong input type");
                Console.ReadLine();
                addCharacterMenu();
            }
            return new Character(0, name, sex, ageInt);
        }
        public Anime editAnimeMenu()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Write anime name: ");
            string animeName = Console.ReadLine();
            Console.Write("Write studio name: ");
            string studioName = Console.ReadLine();
            Console.Write("Write date: ");
            string date = Console.ReadLine();
            int dateInt = new int();
            Console.Write("Write rating: ");
            string rating = Console.ReadLine();
            int ratingInt = new int();
            try
            {
                dateInt = Convert.ToInt32(date);
                ratingInt = Convert.ToInt32(rating);
            }
            catch (Exception)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("Wrong input type");
                Console.ReadLine();
                editAnimeMenu();
            }
            return new Anime(animeName, studioName, dateInt, ratingInt);
        }
        public Character editCharacterMenu()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Write character id: ");
            string id = Console.ReadLine();
            int idInt = new int();
            Console.Write("Write character name: ");
            string name = Console.ReadLine();
            Console.Write("Write character sex: ");
            string sex = Console.ReadLine();
            Console.Write("Write character age: ");
            string age = Console.ReadLine();
            int ageInt = new int();
            try
            {
                idInt = Convert.ToInt32(id);
                ageInt = Convert.ToInt32(age);
            }
            catch (Exception)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("Wrong input type");
                Console.ReadLine();
                editCharacterMenu();
            }
            return new Character(idInt, name, sex, ageInt);
        }
        public string delteAnimeMenu()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Write anime name: ");
            return Console.ReadLine();
        }
        public int deleteCharacterMenu()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Write character id: ");
            string id = Console.ReadLine();
            int idInt = new int();
            try
            {
                idInt = Convert.ToInt32(id);
            }
            catch (Exception)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("Wrong input type");
                Console.ReadLine();
                editCharacterMenu();
            }
            return idInt;
        }
        
    }
}
