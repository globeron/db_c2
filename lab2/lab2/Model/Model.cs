﻿using System;
using System.Collections.Generic;
using System.Text;
using Npgsql;
using lab2.Entity;

namespace lab2.Model
{
    class Model
    {
        private NpgsqlConnection database;
        public Model()
        {
            if (database == null)
            {
                database = new NpgsqlConnection("Host = localhost; Username = postgres; Password = 5175; Database = lab1");
                database.Open();
            }
        }
        public Anime getAnimeByName(string name)
        {
            using var req = new NpgsqlCommand("SELECT * from \"anime\" WHERE \"animeName\" = \'" + name + "\'", database);
            using NpgsqlDataReader item = req.ExecuteReader();
            if (item.Read())
            {
                return new Anime(item.GetString(0), item.GetString(1), item.GetInt32(2), item.GetFloat(3));
            }
            else
                return null;
        }
        public string addAnime(Anime newAnime)//в курсаче сделать throw
        {
            using var req = new NpgsqlCommand("INSERT INTO anime(\"animeName\", \"studioName\", date, rating) VALUES(@animeName, @studioName, @date, @rating)", database);
            req.Parameters.AddWithValue("animeName", newAnime.animeName);
            req.Parameters.AddWithValue("studioName", newAnime.studioName);
            req.Parameters.AddWithValue("date", newAnime.date);
            req.Parameters.AddWithValue("rating", newAnime.rating);
            req.Prepare();
            try
            {
                req.ExecuteNonQuery();
            }
            catch (PostgresException)
            {
                return null;
            }
            using var req2 = new NpgsqlCommand("SELECT \"animeName\" FROM \"anime\" WHERE \"animeName\" = \'" + newAnime.animeName + "\'", database);
            //
            try
            {
                req.ExecuteReader();
            }
            catch (PostgresException)
            {
                return null;
            }
            //
            using NpgsqlDataReader item = req.ExecuteReader();
            if (item.Read())
            {
                return item.GetString(0);
            }
            return null;
        }
        public bool deleteAnime(string animeName)
        {
            bool status = true;
            using var req = new NpgsqlCommand("DELETE from \"anime\" WHERE \"animeName\" = \'" + animeName + "\'", database);
            if (req.ExecuteNonQuery() == -1)
                status = false;
            using var req2 = new NpgsqlCommand("DELETE from \"linkAnimeChar\" WHERE \"animeName\" = \'" + animeName + "\'", database);
            req2.ExecuteNonQuery();
            //
            //В курсаче переделать на каскад
            //
            return status;
        }
        public Anime editAnime(Anime editedAnime)
        {
            using var req = new NpgsqlCommand("UPDATE \"anime\" SET \"studioName\" = @studioName, date = @date, rating = @rating WHERE \"animeName\" = \'" + editedAnime.animeName + "\'", database);
            req.Parameters.AddWithValue("@studioName", editedAnime.studioName);
            req.Parameters.AddWithValue("@date", editedAnime.date);
            req.Parameters.AddWithValue("@rating", editedAnime.rating);
            if (req.ExecuteNonQuery() == -1)
                return null;
            using var req2 = new NpgsqlCommand("SELECT * FROM \"anime\" WHERE \"animeName\" = \'" + editedAnime.animeName + "\'", database);
            using NpgsqlDataReader item = req2.ExecuteReader();
            if (item.Read())
                return new Anime(item.GetString(0), item.GetString(1), item.GetInt32(2), item.GetFloat(3));
            return null;
        }
        public void generatAnime(int num)
        {
            using var req = new NpgsqlCommand("INSERT INTO anime (\"animeName\", \"studioName\", \"date\", rating) SELECT chr(trunc(65 + random()*25)::int) || " +
                "chr(trunc(97 + random() * 25)::int) ||" +
                "chr(trunc(97 + random() * 25)::int) ||" +
                "chr(trunc(97 + random() * 25)::int), \"generatStudioName\"(), trunc(random() * 121 + 1900), trunc(random() * 9 + 1) FROM generate_series(1, @num)", database);
            req.Parameters.AddWithValue("@num", num);
            req.ExecuteNonQuery();
        }


        public int addCharacter(Character newCharacter)
        {
            using var req = new NpgsqlCommand("INSERT INTO character(name, sex, age) VALUES(@name, @sex, @age)", database);
            req.Parameters.AddWithValue("name", newCharacter.name);
            req.Parameters.AddWithValue("sex", newCharacter.sex);
            req.Parameters.AddWithValue("age", newCharacter.age);
            req.Prepare();
            req.ExecuteNonQuery();
            using var req2 = new NpgsqlCommand("SELECT currval(pg_get_serial_sequence('character','charID'));", database);
            using NpgsqlDataReader item = req2.ExecuteReader();
            if(item.Read())
            {
                return item.GetInt32(0);
            }
            return -1;
        }
        public bool deleteCharacter(int id)
        {
            bool status = true;
            using var req = new NpgsqlCommand("DELETE from \"character\" WHERE \"charID\" = " + id, database);
            if (req.ExecuteNonQuery() == -1)
                status = false;
            using var req2 = new NpgsqlCommand("DELETE from \"linkAnimeChar\" WHERE \"charID\" = " + id, database);
            req2.ExecuteNonQuery();
            //
            //В курсаче переделать на каскад
            //
            return status;
        }
        public Character editCharacter(Character editedCharacter)
        {
            using var req = new NpgsqlCommand("UPDATE \"character\" SET name = @name, sex = @sex, age = @age WHERE \"charID\" = " + editedCharacter.charID, database);
            req.Parameters.AddWithValue("@name", editedCharacter.name);
            req.Parameters.AddWithValue("@sex", editedCharacter.sex);
            req.Parameters.AddWithValue("@age", editedCharacter.age);
            if (req.ExecuteNonQuery() == -1)
                return null;
            using var req2 = new NpgsqlCommand("SELECT * from \"character\" WHERE \"charID\" = " + editedCharacter.charID, database);
            using NpgsqlDataReader item = req2.ExecuteReader();
            if (item.Read())
                return new Character(item.GetInt32(0), item.GetString(1), item.GetString(2), item.GetInt32(3));
            return null;
        }
        public Character getCharacterById(int id)
        {
            using var req = new NpgsqlCommand("SELECT * from \"character\" WHERE \"charID\" = " + id, database);
            using NpgsqlDataReader item = req.ExecuteReader();
            if (item.Read())
            {
                return new Character(item.GetInt32(0), item.GetString(1), item.GetString(2), item.GetInt32(3));
            }
            else
                return null;
        }
        /// <summary>
        /// Поиск аниме по дате выхода, полу персонажа и возраста персонажа
        /// </summary>
        public List<Anime> searchAnime1(int ageL, int ageH, string sex, int dateL, int dateH)
        {
            using var req = new NpgsqlCommand("SELECT anime.\"animeName\", anime.\"studioName\", anime.date, anime.rating from \"anime\"" +
                                              "INNER JOIN \"character\" ON((character.age BETWEEN @ageL and @ageH) and(character.sex = @sex))" +
                                              "INNER JOIN \"linkAnimeChar\" ON(\"linkAnimeChar\".\"animeName\" = anime.\"animeName\" and(\"linkAnimeChar\".\"charID\" = character.\"charID\"))" +
                                              "WHERE(anime.date BETWEEN @dateL and @dateH)"
                                              , database);
            req.Parameters.AddWithValue("@ageL", ageL);
            req.Parameters.AddWithValue("@ageH", ageH);
            req.Parameters.AddWithValue("@sex", sex);
            req.Parameters.AddWithValue("@dateL", dateL);
            req.Parameters.AddWithValue("@dateH", dateH);
            using NpgsqlDataReader item = req.ExecuteReader();
            List<Anime> animeList = new List<Anime>();
            while (item.Read())
            {
                animeList.Add(new Anime(item.GetString(0), item.GetString(1), item.GetInt32(2), item.GetFloat(3)));
            }
            return animeList;

        }
        /// <summary>
        /// Поиск аниме по дате выхода, рейтингу и активности студии
        /// </summary>
        public List<Anime> searchAnime2(bool status, int dateL, int dateH, int ratingL, int ratingH)
        {
            using var req = new NpgsqlCommand("SELECT anime.\"animeName\", anime.\"studioName\", anime.date, anime.rating from anime "+ 
                                              "INNER JOIN studio ON(anime.\"studioName\" = studio.name)"+
                                              "WHERE(studio.activity = @actv) and(anime.date BETWEEN @dateL and @dateH) and(anime.rating BETWEEN @ratingL and @ratingH)"
                                              , database);
            req.Parameters.AddWithValue("@actv", status);
            req.Parameters.AddWithValue("@dateL", dateL);
            req.Parameters.AddWithValue("@dateH", dateH);
            req.Parameters.AddWithValue("@ratingL", ratingL);
            req.Parameters.AddWithValue("@ratingH", ratingH);
            using NpgsqlDataReader item = req.ExecuteReader();
            List<Anime> animeList = new List<Anime>();
            while (item.Read())
            {
                animeList.Add(new Anime(item.GetString(0), item.GetString(1), item.GetInt32(2), item.GetFloat(3)));
            }
            return animeList;
        }
        /// <summary>
        /// Поиск персонажа по названию аниме, полу персонажа и возраста
        /// </summary>
        public List<Character> searchCharacter1(string name, string sex, int ageL, int ageH)
        {
            using var req = new NpgsqlCommand("SELECT character.\"charID\", character.name, character.sex, character.age  from character "+
                                              "INNER JOIN anime ON(anime.\"animeName\" LIKE @like) "+
                                              "INNER JOIN \"linkAnimeChar\" ON(\"linkAnimeChar\".\"animeName\" = anime.\"animeName\" and(\"linkAnimeChar\".\"charID\" = character.\"charID\")) "+
                                              "WHERE(character.sex = @sex and(character.age BETWEEN @ageL and @ageH))"
                                              , database);
            req.Parameters.AddWithValue("@like", '%'+name+'%');
            req.Parameters.AddWithValue("@sex", sex);
            req.Parameters.AddWithValue("@ageL", ageL);
            req.Parameters.AddWithValue("@ageH", ageH);
            using NpgsqlDataReader item = req.ExecuteReader();
            List<Character> characterList = new List<Character>();
            while (item.Read())
            {
                characterList.Add(new Character(item.GetInt32(0), item.GetString(1), item.GetString(2), item.GetInt32(3)));
            }
            return characterList;
        }
    }
}