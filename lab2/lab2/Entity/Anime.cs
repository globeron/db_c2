﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab2.Entity
{
    class Anime
    {
        public string animeName;
        public string studioName;
        public int date;
        public float rating;
        public Anime(string animeName, string studioName, int date, float rating)
        {
            this.animeName = animeName;
            this.studioName = studioName;
            this.date = date;
            this.rating = rating;
        }
        public void printAnime()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Name: ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(animeName);
            //
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Studio: ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(studioName);
            //
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Date: ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(date);
            //
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("Rating: ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(rating);
        }
    }
}
